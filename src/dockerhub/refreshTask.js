const puppeteer = require('puppeteer')
const db = require('../db/connect')
const logger = require('../log/logger')
const { chunk } = require('lodash')

const loginDockerHub = async (page, currentUser) => {
  try {
    await page.goto('http://hub.docker.com/sso/start', { waitUntil: 'networkidle0' })

    // 登录
    await page.type('input[id=nw_username]', currentUser.username, { delay: 100 })
    await page.type('input[id=nw_password]', currentUser.mail, { delay: 100 })
    await page.tap('#nw_submit')
    const loginResponse = await page.waitForResponse('https://id.docker.com/api/id/v1/user/login')

    // 检查是否被封号
    if (loginResponse.status() == 401) {
      await db.query('update user set status = 2,forbidden_time=?,`lock`=0 where id = ?', [new Date(), currentUser.id])
      logger.text(`用户 ${currentUser.id} 被封禁`)
      return
    }

    // 检查账号邮箱是否验证成功
    await page.waitForSelector('div[data-testid=topBannerText')
    const verifyStatus = await page.$eval('div[data-testid=topBannerText]', (node) => node.innerText)
    if (verifyStatus.includes('Please check your inbox to verify the emai')) {
      await db.query('delete from user where id=?', [currentUser.id])
      logger.text(`用户 ${currentUser.id} 邮箱未验证成功`)
      return
    }
  } catch (error) {
    throw new Error(error)
  }
}

const run = async () => {
  const [users] = await db.query('select * from user where status != 2 order by create_time desc')

  const run = async (user) => {
    logger.text(`当前用户：${user.username}`)
    const browser = await puppeteer.launch({ headless: true })
    const page = await browser.newPage()
    page.setDefaultNavigationTimeout(60 * 1000)

    try {
      await loginDockerHub(page, user)
      browser.close()
    } catch (error) {
      logger.error(error.stack)
      browser.close()
    }
  }

  for (const userChunks of chunk(users, 40)) {
    await Promise.all(userChunks.map((user) => run(user)))
  }
}

run()
