const puppeteer = require('puppeteer')
const axios = require('axios')
const { CAPTCHA_API_KEY } = require('../config/secret')
const logger = require('../log/logger')
const db = require('../db/connect')
const stringRandom = require('string-random')
const { chunk } = require('lodash')

const MAIL_CNAME = 'mxsubjuejin.juejin.asia'

const getGoogleKey = async (page) => {
  await page.goto('https://hub.docker.com/', {
    waitUntil: 'networkidle0'
  })
  const dimensions = await page.evaluate(() => {
    return {
      url: document.getElementsByTagName('iframe')[0].src
    }
  })
  const googleKeyUrl = new URL(dimensions.url)
  return googleKeyUrl.searchParams.get('k')
}

const getCaptchaResult = async (googleKey) => {
  const requestCaptchaResult = await axios.get(
    `http://2captcha.com/in.php?key=${CAPTCHA_API_KEY}&method=userrecaptcha&googlekey=${googleKey}&pageurl=https://hub.docker.com/&json=1`
  )
  const requestCaptchaResultId = requestCaptchaResult.data.request

  return new Promise((resolve, reject) => {
    let times = 0

    const getResult = async () => {
      if (times > 20) {
        clearInterval(getCaptchaResultInterval)
        reject(new Error('获取验证码返回值超时'))
      }
      times++
      logger.text('获取验证码返回值次数：' + times)
      const result = await axios.get(`http://2captcha.com/res.php?key=${CAPTCHA_API_KEY}&action=get&id=${requestCaptchaResultId}&json=1`)
      if (result.data.status == 1) {
        clearInterval(getCaptchaResultInterval)
        logger.text('验证码返回值：' + result.data.request)
        resolve(result.data.request)
      }
    }

    getResult()
    const getCaptchaResultInterval = setInterval(getResult, 10 * 1000)
  })
}

const sendValidateMail = async (user, recaptcha) => {
  return axios
    .post('https://hub.docker.com/v2/users/signup/', {
      email: user.mail,
      password: user.password,
      recaptcha_response: recaptcha,
      redirect_value: '',
      subscribe: false,
      username: user.username
    })
    .catch((err) => {
      logger.error(err.response.data)
      throw err
    })
}

const getMailResult = async (user) => {
  return new Promise((resolve, reject) => {
    let times = 0
    const getMailInterval = setInterval(async () => {
      if (times > 20) {
        clearInterval(getMailInterval)
        reject(new Error('获取激活邮件超时'))
      }

      times++
      logger.text('获取激活邮件次数：' + times)
      const [result] = await db.query('select * from mail where username = ? order by create_time desc', [user.username])

      const confirmMailTitle = '[Docker Hub] Please confirm your email address'
      const confirmMail = result.find((v) => v.subject == confirmMailTitle)
      if (confirmMail) {
        const mailDetail = confirmMail.text
        console.log(mailDetail)
        const validateUrl = mailDetail.match(/isit([\s\S]*?)to/)[1].trim()

        resolve(validateUrl)
        clearInterval(getMailInterval)
      }
    }, 5 * 1000)
  })
}

const loginAndValidate = async (page, currentUser, url) => {
  await page.goto('http://hub.docker.com/sso/start', {
    waitUntil: 'networkidle0'
  })

  await page.type('input[id=nw_username]', currentUser.username)
  await page.type('input[id=nw_password]', currentUser.mail)
  await page.tap('#nw_submit')
  await page.waitForNavigation()

  await page.goto(url, {
    waitUntil: 'networkidle0'
  })
  await page.waitForTimeout(1000)
}

const register = async () => {
  const browser = await puppeteer.launch({ headless: true })
  const page = await browser.newPage()
  page.setDefaultNavigationTimeout(60 * 1000)

  try {
    const googleKey = await getGoogleKey(page)
    const captchaResult = await getCaptchaResult(googleKey)
    const randomMail = `${stringRandom()}@${MAIL_CNAME}`.toLowerCase()
    logger.text(`随机邮箱：${randomMail}`)

    const user = {
      username: randomMail.split('@')[0],
      mail: randomMail,
      password: randomMail
    }

    const sendValidateMailResult = await sendValidateMail(user, captchaResult)
    if (!sendValidateMailResult.data) {
      throw new Error('用户注册失败')
    }

    // const user = {
    //   mail: '4x6wsw8h@uuf.me'
    // }

    const validateUrl = await getMailResult(user)

    await loginAndValidate(page, user, validateUrl)
    await db.insert('user', user)
    logger.info(`用户${user.username}注册成功`)
    browser.close()
  } catch (error) {
    logger.error(error.stack)
    browser.close()
  }
}

const run = async () => {
  for (const tasks of chunk(Array.from({ length: 250 }), 20)) {
    await Promise.all(tasks.map(() => register()))
  }
  logger.info('task finished')
}

run()
